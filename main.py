import argparse

# Import simulation object
from src.sailsimuserver import SailSimuServer

if __name__ == "__main__":

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--nooboat', help="set the number of nooboat", type=int, default=10, required=False)
    args = parser.parse_args()

    # Init the server
    server = SailSimuServer(args.nooboat)

    # Run the simulation
    server.start()
