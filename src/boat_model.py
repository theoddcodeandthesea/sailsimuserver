import numpy as np

from .utils import *

# Speed table
S_TABLE = [
    0.0,
    0.04347826086956522,
    0.2173913043478261,
    0.47826086956521746,
    0.5652173913043479,
    0.673913043478261,
    0.8478260869565218,
    0.8913043478260869,
    0.9347826086956522,
    0.9782608695652175,
    1.0,
    0.9782608695652175,
    0.9782608695652175,
    0.8913043478260869,
    0.8260869565217391,
    0.6956521739130436,
    0.6521739130434783,
    0.5869565217391305,
    0.5434782608695653
]

class BoatModel:
    def __init__(self, pos):
        self.reset(pos)

        self.last_ts = 0.0

    def reset(self, pos):
        self.pos = pos
        self.speed = np.zeros((2), dtype="float")

        self.roll = 0.0
        self.pitch = 0.0
        self.yaw = 0.0

        self.rudder_angle = 0.0
        self.sail_angle = 0.0

    def update(self, ts, pilot_cmd, local_wind, local_current):
        raise("The function update() of the BoatModel class should be overwritten.")

class EasyModel(BoatModel):
    def __init__(self, pos):
        super(EasyModel, self).__init__(pos)

    def update(self, ts, pilot_cmd, local_wind, local_current):
        # Calculate dt
        dt = ts - self.last_ts
        self.last_ts = ts

        # Extract wind data
        wind_direction = np.arctan2(local_wind[1], local_wind[0])
        wind_speed = np.linalg.norm(local_wind)

        # Update speed
        # If the sail is orced to shiver, speed is zero, else, depends of the wind boat angle
        if pilot_cmd["shiver"]:
            self.speed = np.zeros((2), dtype="float")
        else:
            wind_yaw_angle = get_angle_around(np.pi - wind_direction - self.yaw, 0)
            wind_deg = np.degrees(wind_yaw_angle)
            i = abs(wind_deg) / 10
            i0 = int(round(i))
            coef = 0
            if i0 >= len(S_TABLE) - 1:
                coef = S_TABLE[-1]
            else:
                k = (i - i0)
                coef = S_TABLE[i0] + (S_TABLE[i0 + 1] - S_TABLE[i0]) * k
            self.speed = np.array([np.cos(self.yaw), np.sin(self.yaw)]) * coef * wind_speed

        # Add drift speed
        drift_coef = 0.05
        # self.speed += np.array([np.cos(wind_direction), np.sin(wind_direction)]) * drift_coef * wind_speed

        # Update rudder angle
        rudder_speed = np.pi/2 # 90 °/s
        rudder_step = rudder_speed * dt
        if self.rudder_angle != pilot_cmd["rudder"]:
            if self.rudder_angle < pilot_cmd["rudder"] - rudder_step:
                self.rudder_angle += rudder_step
            elif self.rudder_angle > pilot_cmd["rudder"] + rudder_step:
                self.rudder_angle -= rudder_step
            else:
                self.rudder_angle = pilot_cmd["rudder"]

        # Update yaw
        dy = self.rudder_angle * dt * 0.5
        self.yaw += dy

        # Update position with the boat speed
        self.pos += dt * self.speed

        return self.pos, self.speed, self.roll, self.pitch, self.yaw
