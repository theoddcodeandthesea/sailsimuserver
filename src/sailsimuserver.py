import time
import numpy as np

from .eole import Eole
from .displayer import LocalDisplayer
from .boat import Boat
from .buoy import Buoy

# Size of the map
WIDTH = 1000
HEIGHT = 1000

class SailSimuServer:
    def __init__(self, nooboat_nb):

        # Init the list of all boats
        self.boats = []
        # Init the start position
        self.start_pos = np.array([50, 500], dtype="float")

        # Init the race
        self.buoys = []
        valid_dist = 5
        self.buoys.append(Buoy(800, 500, valid_dist))
        self.buoys.append(Buoy(800, 300, valid_dist))
        self.buoys.append(Buoy(50, 300, valid_dist))
        self.buoys.append(Buoy(50, 500, valid_dist))
        self.buoys.append(Buoy(800, 500, valid_dist))
        self.buoys.append(Buoy(800, 300, valid_dist))
        self.buoys.append(Buoy(50, 300, valid_dist))
        self.buoys.append(Buoy(50, 500, valid_dist))

        # Init Nooboats
        if nooboat_nb > 0:
            self.boats.append(Boat("ChampiNoob", self.start_pos, Boat.PILOT_MODE_CHAMPINOOB, Boat.MODEL_LVL_EASY, self.buoys))
            # Create one nooboat better that the others
            for i in range(nooboat_nb - 1):
                self.boats.append(Boat("Noob_"+str(i), self.start_pos, Boat.PILOT_MODE_NOOBOAT, Boat.MODEL_LVL_EASY, self.buoys))

        # TMP : set the player as a champnoob to make the simu works
        self.player = Boat("Player", self.start_pos, Boat.PILOT_MODE_CHAMPINOOB, Boat.MODEL_LVL_EASY, self.buoys)
        self.boats.append(self.player)

        # Init displayer with the buoys
        self.displayer = LocalDisplayer(0.05, self.buoys, WIDTH, HEIGHT)

        # At the begin of each simulation, init wind manager
        self.eole = Eole(np.radians(180), 10, WIDTH, HEIGHT, 100)

    def start(self):
        # Start the displayer
        self.displayer.start()

        # Set the dt (delta time between two step of simulation)
        dt = 0.1
        # Init the timestamp at 0 (time spent since the begining of this simulation in seconds)
        ts = 0
        # Init the speed of the simulation (1 for realtime, 2 for twice the realtime speed)
        speed = 50
        # Set a timeout in case the player fail to finish the course
        timeout = 6000000 # TODO: [EC] Find a more realistic value and add a option to set it

        # Start simulation
        # Run the simulation while the timeout is not reached,
        #  the player haven't finish the course or closed the window
        while ts <= timeout and not self.player.race_finished and self.displayer.is_alive():
            # Update the timestamp
            ts += dt

            # Update wind
            self.eole.update(ts)

            # Update data of every boat
            for boat in self.boats:
                # Get wind for this boat
                wind = self.eole.get_wind_at(boat.get_state()[0])

                # Update the boat
                boat.update(ts, wind, None, self.boats)

            # Display
            self.displayer.display(ts, self.boats, self.eole.get_wind_grid())

            # Sleep the necessary time
            time.sleep(float(dt)/speed)

        # Wait that the player close the window
        while self.displayer.is_alive():
            self.displayer._check_closing()
            time.sleep(0.1)
