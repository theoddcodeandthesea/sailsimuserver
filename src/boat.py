from .boat_pilot import *
from .boat_model import *

class Boat:
    PILOT_MODE_NOOBOAT = "NoobBoat"
    PILOT_MODE_CHAMPINOOB = "ChampiNoob"
    PILOT_MODE_NETWORK = "Network"

    MODEL_LVL_EASY = 0

    def __init__(self, name, start_pos, pilot_mode, model_level, buoys, socket=None):

        if pilot_mode == self.PILOT_MODE_NOOBOAT:
            self.pilot = NoobPilot()
        elif pilot_mode == self.PILOT_MODE_CHAMPINOOB:
            self.pilot = ChampiNoobPilot()
        elif pilot_mode == self.PILOT_MODE_NETWORK:
            self.pilot = NetworkPilot(socket)
        else:
            raise("Unknown pilot mode: " + str(pilot_mode))

        if model_level == self.MODEL_LVL_EASY:
            self.model = EasyModel(start_pos.copy()) # The copy is needed
        else:
            raise("Unknown model level: " + str(model_level))

        self.buoys = buoys
        self.buoy_index = 0
        self.race_finished = False

        self.name = name

    def reset(self, pos):
        self.model.reset(pos)

        self.buoy_index = 0
        self.race_finished = False

    def get_state(self):
        return self.model.pos, self.model.speed, self.model.roll, self.model.pitch, self.model.yaw

    def get_cmd(self):
        return self.pilot.cmd["rudder"], self.pilot.cmd["sail"], self.pilot.cmd["shiver"]

    def validate_buoy(self):
        if self.buoy_index < len(self.buoys):
            if np.linalg.norm(self.buoys[self.buoy_index].pos - self.model.pos) < self.buoys[self.buoy_index].valid_dist:
                self.buoy_index += 1

        if len(self.buoys) and self.buoy_index >= len(self.buoys):
            self.buoy_index = 0
            self.race_finished = True

    def update(self, ts, local_wind, local_current, boats):
        # If necessary, validate the actual buoy and update the next one
        self.validate_buoy()

        # Update the model
        state = self.model.update(ts, self.pilot.cmd, local_wind, local_current)

        # Give the boat state to the pilot
        self.pilot.set_state(*state)

        # Set pilot next buoy observations
        buoy_bearing = get_bearing(self.model.pos, self.buoys[self.buoy_index].pos)
        buoy_distance = np.linalg.norm(self.buoys[self.buoy_index].pos - self.model.pos)
        self.pilot.set_buoy(buoy_bearing, buoy_distance)

        # Give boats data to the pilot
        for boat in boats:
            if boat != self:
                bearing = get_bearing(state[0], state[0])
                boat_state = boat.get_state()
                distance = np.linalg.norm(boat_state[0] - state[0])
                self.pilot.set_boat_data(boat.name, bearing, distance, boat_state[1] - state[1], boat_state[2], boat_state[3], boat_state[4])

        # Give local wind to the pilot
        self.pilot.set_wind(local_wind)

        # Give the boat's water speed in its yaw axis
        # TODO: Do the calculation to get this info
        self.pilot.set_water_speed(np.linalg.norm(state[1]))

        # Update the pilot data, it should cause the calculation of new orders
        self.pilot.update()
