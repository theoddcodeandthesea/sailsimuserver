import numpy as np

class Puff():
    def __init__(self, pos, width, height, orientation, speed, direction):
        self.pos = pos
        self.width = width
        self.height = height
        self.orientation = orientation
        self.speed = speed
        self.direction = direction

class Eole:
    def __init__(self, direction, speed, width, height, step):
        self.wind = np.array([np.cos(direction), np.sin(direction)], dtype="float") * speed

        self.width = width
        self.height = height
        self.step = step

        self.last_ts = 0

        self.puffs = []
        self.puff_grid = np.zeros((int(height/step), int(width/step), 2), dtype="float")

        self.create_puff()
        self.create_puff()

    def get_wind_at(self, pos):
        x = int(pos[0] / self.step) - 1
        y = int(pos[1] / self.step) - 1
        local_puffs = self.puff_grid[x, y]
        return self.wind + local_puffs

    def get_wind_grid(self):
        return self.puff_grid + self.wind

    def update(self, ts):
        dt = ts - self.last_ts
        self.last_ts = ts
        self.move_puffs(dt)

    def move_puffs(self, dt):
        self.puff_grid = np.zeros((int(self.height/self.step), int(self.width/self.step), 2), dtype="float")

        for puff in self.puffs:
            wind_dir = np.arctan2(self.wind[1], self.wind[0])
            wind_speed = np.sqrt(self.wind[0]**2 + self.wind[1]**2)
            puff.pos += np.array([np.cos(puff.direction + wind_dir), np.sin(puff.direction + wind_dir)]) * wind_speed * dt

            # 1/3 full, rest prop

            for i in range(int(self.height/self.step)):
                x = int((i+0.5) * self.step)

                for j in range(int(self.width/self.step)):
                    y = int((j+0.5) * self.step)

                    x_coef = 0
                    x_diff = abs(x - puff.pos[0])
                    if x_diff < puff.height / 6:
                        x_coef = 1
                    elif x_diff < puff.height / 2:
                        x_coef = (x_diff - puff.height / 2) / (puff.height / 6 - puff.height / 2)

                    y_coef = 0
                    y_diff = abs(y - puff.pos[1])
                    if y_diff < puff.width / 6:
                        y_coef = 1
                    elif y_diff < puff.width / 2:
                        y_coef = (y_diff - puff.width / 2) / (puff.width / 6 - puff.width / 2)

                    coef = x_coef * y_coef

                    dir = wind_dir + puff.direction * coef
                    speed = puff.speed * coef
                    puff_vec = np.array([np.cos(dir), np.sin(dir)]) * speed
                    self.puff_grid[i, j] += puff_vec

            if puff.pos[0] < -self.height / 2:
                self.puffs.remove(puff)
                self.create_puff()

    def create_puff(self):
        wind_dir = np.pi - np.arctan2(self.wind[1], self.wind[0])

        width = np.random.randint(300, 600)
        height = np.random.randint(300, 600)
        pos = np.array([self.height, self.width], dtype="float") / 2 \
            + np.array([np.cos(wind_dir), np.sin(wind_dir)]) * max(self.height, self.width)/2 \
            + np.array([0, np.random.randint(-self.width/2, self.width/2)])
        orientation = np.radians(np.random.randint(0, 90))
        speed = np.random.randint(20, 80)/10.
        direction = np.radians(np.random.randint(-20, 20))

        self.puffs.append(Puff(pos, width, height, orientation, speed, direction))
