import numpy as np

from .utils import *

# 1/TACK_CHANCE is the probability that a Nooboat do a tack
TACK_CHANCE = 1000

# Mother class of every Pilot
class BoatPilot:
    def __init__(self, cmd_callback=None):
        # Callback runned when a cmd calculation is finished
        self.cmd_callback = cmd_callback

        # Init all the observations the pilot can have
        self._obs = {
            "pos": np.zeros((2), dtype="float"),
            "speed": np.zeros((2), dtype="float"),
            "yaw": 0,
            "pitch": 0,
            "roll": 0,

            "water_speed": 0,

            "wind": np.zeros((2), dtype="float"),

            "buoy_bearing": 0,
            "buoy_distance": 0,

            "boats": {}
        }
        '''
        Example for boats dictionnary :
        {
            "boat_name_1": {
                "bearing": 0,
                "distance": 0,
                "relative_speed": 0,
                "roll": 0,
                "pitch": 0,
                "yaw": 0
            }
        }
        '''

        # Init all cmd
        self.cmd = {
            "rudder": 0,
            "sail": 0,
            "shiver": False
        }


    # Function called to update the decisions of the pilot
    def update(self):
        raise("The update function of the AutoPilot class need to be overwritten !")

    def set_state(self, pos, speed, roll, pitch, yaw):
        self._obs["pos"] = pos
        self._obs["speed"] = speed
        self._obs["roll"] = roll
        self._obs["pitch"] = pitch
        self._obs["yaw"] = yaw

    def set_water_speed(self, water_speed):
        self._obs["water_speed"] = water_speed

    def set_wind(self, wind):
        self._obs["wind"] = wind

    def set_buoy(self, bearing, distance):
        self._obs["buoy_bearing"] = bearing
        self._obs["buoy_distance"] = distance

    def set_boat_data(self, name, bearing, distance, relative_speed, roll, pitch, yaw):
        self._obs["boats"][name] = {
            "bearing": bearing,
            "distance": distance,
            "relative_speed": relative_speed,
            "roll": roll,
            "pitch": pitch,
            "yaw": yaw
        }


# Example of a noob pilot
class NoobPilot(BoatPilot):
    def __init__(self):
        super(NoobPilot, self).__init__()
        self.yaw_setpoint = None # Yaw target
        self.up_wind_angle = np.radians(30) # Angle min between the boat and the wind

    def update(self):
        # Wait that the previous yaw target have been reached to decide of an other one
        if self.yaw_setpoint is None or abs(self.yaw_setpoint - self._obs["yaw"]) < np.radians(5):

            # Set the bearinf to the next buoy as the yaw setpoint
            self.yaw_setpoint = get_angle_around(self._obs["buoy_bearing"], self._obs["yaw"])

            # Prepare the wind angle to be able to compare it to the yaw
            wind_direction = np.pi-np.arctan2(self._obs["wind"][1], self._obs["wind"][0])
            wind = get_angle_around(wind_direction, self._obs["yaw"])
            # Get the angle between the wind and the desired yaw in [-PI;PI]
            wind_dest = get_angle_around(wind - self.yaw_setpoint, 0)
            # If this angle is smaller than the up_wind_angle, change the yaw setpoint
            if abs(wind_dest) < self.up_wind_angle:
                # Get an hazard coef equals to 1 or -1
                hazard_coef = 1 if np.random.randint(TACK_CHANCE) != 0 else -1
                # Choose the closest leg to the actual yaw and use the hazard coef to do a tack sometimes
                if abs(wind - self.up_wind_angle - self._obs["yaw"]) > abs(wind + self.up_wind_angle - self._obs["yaw"]):
                    self.yaw_setpoint = wind + self.up_wind_angle * hazard_coef
                else:
                    self.yaw_setpoint = wind - self.up_wind_angle * hazard_coef

        # Decide of the helm angle depending of the diff between the yaw and the setpoint
        if abs(self.yaw_setpoint - self._obs["yaw"]) < np.radians(2):
            self.cmd["rudder"] = 0
        elif self.yaw_setpoint > self._obs["yaw"]:
            self.cmd["rudder"] = np.radians(20)
        else:
            self.cmd["rudder"] = np.radians(-20)

        if self.cmd_callback is not None:
            self.cmd_callback()

# A Noob a little less bad than the other ones
class ChampiNoobPilot(NoobPilot):
    def __init__(self):
        super(ChampiNoobPilot, self).__init__()
        self.up_wind_angle = np.radians(40)
