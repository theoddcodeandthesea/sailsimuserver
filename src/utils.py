import numpy as np

# Calculate the bearing between the positions "from" an "to"
def get_bearing(from_, to):
    dx = to[0] - from_[0]
    dy = to[1] - from_[1]
    return np.arctan2(dy, dx)

# Find the angle + k.2.PI closest to the target (usefull to compare to angles)
def get_angle_around(angle, target):
    while angle < target - np.pi:
        angle += 2 * np.pi
    while angle > target + np.pi:
        angle -= 2 * np.pi
    return angle
